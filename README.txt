INTRODUCTION
------------

Send messages through the SMS Framework using AWS SNS.


REQUIREMENTS
------------

The following modules are required:

 * SMS Framework (https://www.drupal.org/project/smsframework)
 * Mobile Number (https://www.drupal.org/project/mobile_number)

The AWS SDK for PHP v3.x (https://github.com/aws/aws-sdk-php/releases) is
required.
PHP 5.5+ is required. The AWS SDK will not work on earlier versions.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for
further information.
Install composer manager and follow its instructions for installing the AWS
SDK PHP library. The composer.json file included with this module will set the
version to the latest 3.x.


CONFIGURATION
-------------
Configure the module at admin/smsframework/gateways/sms_sns.


MAINTAINERS
-----------

Current maintainers:
 * Jacob Embree (jacobembree) - https://www.drupal.org/u/jacobembree
